type {{ $.InterfaceName }} interface {
{{range .MethodSet}}
	{{.Name}}(context.Context, *{{.Request}}) (*{{.Reply}}, error)
{{end}}
}
func Register{{ $.InterfaceName }}(r gin.IRouter, srv {{ $.InterfaceName }}) {
	s := {{.Name}}{
		server: srv,
		router:     r,
		resp: default{{$.Name}}Resp{},
	}
	s.RegisterService()
}

type {{$.Name}} struct{
	server {{ $.InterfaceName }}
	router gin.IRouter
	resp  interface {
		Error(ctx *gin.Context, err error)
		ParamsError (ctx *gin.Context, err error)
		Success(ctx *gin.Context, data interface{})
	}
}

// Resp 返回值
type default{{$.Name}}Resp struct {}

func (resp default{{$.Name}}Resp) response(ctx *gin.Context, status int,  data interface{}) {
	switch data.(type) {
	case string:
		ctx.String(status, data.(string))
	default:
		ctx.JSON(status, data)
	}
}

// Error 返回错误信息
func (resp default{{$.Name}}Resp) Error(ctx *gin.Context, err error) {
	code := 10500
	msg := "未知错误"
	
	if err == nil {
		msg += ", err is nil"
		resp.response(ctx, 200, map[string]interface{}{
			"code": code,
			"msg":  msg,
		})
		return
	}

	type iCode interface{
		GetCode() int
		GetMsg() string
	}
	log.Printf("{{$.Name}} error: %s", err.Error())
	Unwrap:=func (err error) error {
		u, ok := err.(interface {
			Unwrap() error
		})
		if !ok {
			return err
		}
		return u.Unwrap()
	}
	err=Unwrap(err)
	var c iCode
	if errors.As(err, &c) {
		code = c.GetCode()
		msg = c.GetMsg()
	}else{
		code = 10501
		msg = err.Error()
	}

	_ = ctx.Error(err)

	resp.response(ctx, 200,  map[string]interface{}{
		"code": code,
		"msg": msg,
	})
}

// ParamsError 参数错误
func (resp default{{$.Name}}Resp) ParamsError (ctx *gin.Context, err error) {
	_ = ctx.Error(err)
	resp.response(ctx, 200,  map[string]interface{}{
		"code": 400,
		"msg": "参数错误",
	})
}

// Success 返回成功信息
func (resp default{{$.Name}}Resp) Success(ctx *gin.Context, data interface{}) {
	resp.response(ctx, 200, map[string]interface{}{
		"code": 0,
		"msg": "success",
		"data": data,
	})
}

{{range .Methods}}
func (s *{{$.Name}}) {{ .HandlerName }} (ctx *gin.Context) {
	var in {{.Request}}
{{if .HasPathParams }}
	if err := ctx.ShouldBindUri(&in); err != nil {
		s.resp.ParamsError(ctx, err)
		return
	}
{{end}}
{{if eq .Method "GET" "DELETE" }}
	if err := ctx.ShouldBindQuery(&in); err != nil {
		s.resp.ParamsError(ctx, err)
		return
	}
{{else if eq .Method "POST" "PUT" }}
	if err := ctx.ShouldBindJSON(&in); err != nil {
		s.resp.ParamsError(ctx, err)
		return
	}
{{else}}
	if err := ctx.ShouldBind(&in); err != nil {
		s.resp.ParamsError(ctx, err)
		return
	}
{{end}}
	out, err := s.server.({{ $.InterfaceName }}).{{.Name}}(ctx, &in)
	if err != nil {
		s.resp.Error(ctx, err)
		return
	}
	{{if eq .Reply "emptypb.Empty"}}
	   _=out
	   return
	{{else}}
	   s.resp.Success(ctx, out)
	{{end}}
}
{{end}}

func (s *{{$.Name}}) RegisterService() {
{{range .Methods}}
		s.router.Handle("{{.Method}}", "{{.Path}}", s.{{ .HandlerName }})
{{end}}
}